# import http . server
# import socketserver
# import json


# class MyHttpRequestHandler (http . server . SimpleHTTPRequestHandler):
#     def do_GET(self):
#         if self . path == '/api':
#             self . send_response(200)
#             self . send_header(" Content - type ", " text / html ")
#             self . end_headers()
#             content = json . dumps(" { ? content ? : ? ? response ?? } ")
#             self . wfile . write(bytes(content, " utf8 "))
#             return


# # Create an object of the above class
# handler_object = MyHttpRequestHandler

# PORT = 8000
# my_server = socketserver . TCPServer((" ", PORT), handler_object)

# # Star the server
# my_server . serve_forever()

import flask
from flask import request
import pandas as pd
import json

tasks = [
        {
            'id': 1,
            'name': "task1",
            "description": "This is task 1"
        },
        {
            "id": 2,
            "name": "task2",
            "description": "This is task 2"
        },
        {
            "id": 3,
            "name": "task3",
            "description": "This is task 3"
        }
    ]

app = flask.Flask(__name__)

@app.route("/", methods=['GET'])
def hello():
    # return "hello world!"
    try:
        return json.dumps(tasks)
    except KeyError:
        return "Erro!"

if __name__ == "__main__":
    app.run(host="0.0.0.0")